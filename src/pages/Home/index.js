import React from 'react';
import {Dimensions, StyleSheet, Text, View, ImageBackground, Image,ScrollView} from 'react-native';
import { ImageHeader,Logo } from '../../assets';
import {Saldo,PesananAktif} from '../../components'
import { ButtonIcon } from '../../components';
import { WARNA_ABU_ABU, WARNA_UTAMA } from '../../utils/constant';

const Home = () => {
  return (
    <View style={styles.page}>
      <ScrollView showVerticalScrollIndicator={false}>
        <ImageBackground source={ImageHeader} style={styles.header}>
          <Image source={Logo} style={styles.logo}/>
          <View style={styles.hello}>
            <Text style={styles.selamat}>Selamat Datang</Text>
            <Text style={styles.username}>afifbasya</Text>
          </View>
        </ImageBackground>
        <Saldo/>
        <View style={styles.layanan}>
          <Text style={styles.label}>Layanan Kami</Text>
          <View style={styles.garisBawah}/>
          <View style={styles.iconLayanan}>
            <ButtonIcon title="Kiloan" type="layanan"/>
            
            <ButtonIcon title="Satuan" type="layanan"/>
            
            <ButtonIcon title="VIP" type="layanan"/>
            
            <ButtonIcon title="Karpet" type="layanan"/>
            
            <ButtonIcon title="Setrika Saja" type="layanan"/>
            
            <ButtonIcon title="Ekspress" type="layanan"/>
          </View>
          
        </View>
        <View style={styles.pesananAktif}>
            <Text style={styles.label}>Pesanan Aktif</Text>
            <View style={styles.garisBawah}/>
            <PesananAktif title="Pesanan No. 0002142" status="Sudah Selesai"/>
            <PesananAktif title="Pesanan No. 0002142" status="Masih Dicuci"/>
            <PesananAktif title="Pesanan No. 0002142" status="Sudah Selesai"/>
            <PesananAktif title="Pesanan No. 0002142" status="Sudah Selesai"/>

        </View>
      </ScrollView>
    </View>
  );
};

export default Home;

const windowWidth=Dimensions.get('window').width;
const windowHeight=Dimensions.get('window').height;

const styles = StyleSheet.create({
  page:{
    flex:1,
    backgroundColor:'white'
  },
  header:{
    width:windowWidth,
    height:windowHeight*0.30,
    paddingHorizontal:30,
    paddingTop:10
  },
  logo:{
    width:windowWidth*0.25,
    height:windowHeight*0.06
  },
  hello:{
    marginTop:windowHeight*0.030,
  },
  selamat:{
    fontSize:24,
    fontFamily:'TitilliumWeb-Regular'
  },
  username:{
    fontSize:22,
    fontFamily:'TitilliumWeb-Bold'
  },
  layanan:{
    paddingLeft:30,
    padding:15
  },
  label:{
    fontSize:18,
    fontFamily:'TitilliumWeb-Bold',
    paddingBottom:1
  },
  garisBawah:{
    borderBottomWidth:2,
    borderBottomColor:WARNA_UTAMA,
    width:windowWidth*0.16,
    marginBottom:5
  },
  iconLayanan:{
    flexDirection:'row',
    justifyContent:'space-between',
    marginTop:10,
    flexWrap:'wrap'
  },
  pesananAktif:{
    paddingHorizontal:30,
    backgroundColor:WARNA_ABU_ABU,
    flex:1,
    borderTopRightRadius:20,
    borderTopLeftRadius:20,
    paddingTop:10,
    borderTopColor:WARNA_UTAMA,
    borderTopWidth:3,

  }
});
