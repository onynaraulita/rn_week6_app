import React, {useEffect} from 'react';
import {StyleSheet, Text, View, ImageBackground, Image} from 'react-native';
import {SplashBackground, Logo} from '../../assets';

const Splash = ({navigation}) => {
  //perintah yg akan dijalankan pertama kali
  useEffect(() => {
    setTimeout(() => {
      navigation.replace('MainApp'); //kalo replace, diback akan hilang. kalo navigate, di back akan ke splashscreen
    }, 1000);
  }, [navigation]);

  return (
    <ImageBackground source={SplashBackground} style={styles.background}>
      <Image source={Logo} style={styles.logo} />
    </ImageBackground>
  );
};

export default Splash;

const styles = StyleSheet.create({
  background: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  logo: {
    width: 222,
    height: 105,
  },
});
